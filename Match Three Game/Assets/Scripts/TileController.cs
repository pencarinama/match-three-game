﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : MonoBehaviour
{

    public int id;

    private BoardManager boardManager;
    private SpriteRenderer sprite;

    private GameFlowManager game;

    private static readonly Color selectedColor = new Color(0.5f, 0.5f, 0.5f);
    private static readonly Color normalColor = Color.white;

    private static TileController previousSelected = null;

    private static readonly float moveDuration = 0.5f;
    private static readonly float destroyBigDuration = .1f;
    private static readonly float destroySmallDuration = .4f;

    private static readonly Vector2 sizeBig = Vector2.one * 1.2f;
    private static readonly Vector2 sizeSmall = Vector2.zero;
    private static readonly Vector2 sizeNormal = Vector2.one;


    private bool isSelected = false;

    private static readonly Vector2[] adjacentDirection = new Vector2[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right };


    public bool isSwapping;
    public bool isProcessing;

    public bool isDestroy { get; set; }

    
    private void Awake()
    {
        boardManager = BoardManager.Instance;
        sprite = GetComponent<SpriteRenderer>();
        game = GameFlowManager.Instance;
    }

    public void ChangeId(int id, int x, int y)
    {
        sprite.sprite = boardManager.tileTypes[id];
        this.id = id;

        name = "TILE_" + id + "(" + x + "," + y + ")";
    }

    private void OnMouseDown()
    {
        if(sprite.sprite == null || boardManager.isSwapping || game.isGameOver)
        {
            return;
        }

        SoundManager.Instance.PlayTap();


        if (isSelected)
        {
            Deselect();
        }
        else
        {
            if(previousSelected == null)
            {
                Select();
            }
            else
            {

                if(GetAllAdjacentTiles().Contains(previousSelected))
                {
                    TileController otherTile = previousSelected;
                    previousSelected.Deselect();

                    SwapTile(otherTile, () =>
                    {
                        if(boardManager.GetAllMatches().Count > 0)
                        {
                            Debug.Log("Matched");
                            boardManager.Process();
                        }
                        else
                        {
                            SwapTile(otherTile);
                            SoundManager.Instance.PlayWrong();

                        }
                    });
                }
                else
                {
                    previousSelected.Deselect();
                    Select();
                }

                //previousSelected.Deselect();
                //Select();
            }
        }
    }


    public void Select()
    {
        isSelected = true;
        sprite.color = selectedColor;
        previousSelected = this;
    }
    public void Deselect()
    {
        isSelected = false;
        sprite.color = normalColor;
        previousSelected = null;
    }

    public void SwapTile(TileController otherTile, System.Action OnCompleted = null)
    {
        StartCoroutine(boardManager.SwapTilePosition(this, otherTile, OnCompleted));
    }

    public IEnumerator MoveTilePosition(Vector2 targetPosition, System.Action OnCompleted)
    {
        Vector2 startPosition = transform.position;
        float time = 0f;

        yield return new WaitForEndOfFrame();

        while(time < moveDuration)
        {
            transform.position = Vector2.Lerp(startPosition, targetPosition, time / moveDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.position = targetPosition;
        OnCompleted?.Invoke();
    }

    private TileController GetAdjacent(Vector2 castDir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir, sprite.size.x);

        if(hit)
        {
            return hit.collider.GetComponent<TileController>();
        }
        return null;
    }

    public List<TileController> GetAllAdjacentTiles()
    {
        List<TileController> adjacentTiles = new List<TileController>();

        for(int i = 0; i < adjacentDirection.Length; i++)
        {
            adjacentTiles.Add(GetAdjacent(adjacentDirection[i]));
        }

        return adjacentTiles;
    }


    private List<TileController> GetMatch(Vector2 castDir)
    {
        List<TileController> matchingTiles = new List<TileController>();
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir, sprite.size.x);

        while(hit)
        {
            TileController otherTile = hit.collider.GetComponent<TileController>();
            if(otherTile.id != id || otherTile.isDestroy)
            {
                break;
            }
            matchingTiles.Add(otherTile);
            hit = Physics2D.Raycast(otherTile.transform.position, castDir, sprite.size.x);
        }

        return matchingTiles;
    }

    private List<TileController> GetOneLineMatch(Vector2[] paths)
    {
        List<TileController> matchingTiles = new List<TileController>();

        for(int i = 0; i < paths.Length; i++)
        {
            matchingTiles.AddRange(GetMatch(paths[i]));
        }

        if(matchingTiles.Count >= 2)
        {
            return matchingTiles;
        }

        return null;
    }

    public List<TileController> GetAllMatches()
    {
        if(isDestroy)
        {
            return null;
        }

        List<TileController> matchingTiles = new List<TileController>();
        List<TileController> horizontalMatchingTiles = GetOneLineMatch(new Vector2[2] { Vector2.up, Vector2.down });
        List<TileController> verticalMatchingTiles = GetOneLineMatch(new Vector2[2] { Vector2.left, Vector2.right });

        if(horizontalMatchingTiles != null)
        {
            matchingTiles.AddRange(horizontalMatchingTiles);
        }
        if(verticalMatchingTiles != null)
        {
            matchingTiles.AddRange(verticalMatchingTiles);
        }

        if(matchingTiles != null && matchingTiles.Count >= 2)
        {
            matchingTiles.Add(this);
        }
        return matchingTiles;
    }

    public IEnumerator SetDestroyed(System.Action OnCompleted)
    {
        isDestroy = true;
        id = -1;
        name = "TILE_NULL";

        Vector2 startSize = transform.localScale;
        float time = 0f;


        while(time < destroyBigDuration)
        {
            transform.localScale = Vector2.Lerp(startSize, sizeBig, time / destroyBigDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.localScale = sizeSmall;
        sprite.sprite = null;

        OnCompleted?.Invoke();
    }

    public void GenerateRandomTile(int x, int y)
    {
        transform.localScale = sizeNormal;
        isDestroy = false;

        ChangeId(Random.Range(0, boardManager.tileTypes.Count), x, y);
    }

    // Start is called before the first frame update
    void Start()
    {
        isSwapping = false;
        isProcessing = false;
        isDestroy = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
