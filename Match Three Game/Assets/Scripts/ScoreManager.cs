﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScoreManager : MonoBehaviour
{
    private static int _highScore;
    private static ScoreManager _instance = null;
    public static ScoreManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<ScoreManager>();

                if(_instance == null)
                {
                    Debug.LogError("Fatal Error: Score Manager not found");

                }
            }
            return _instance;
        }
    }


    public int tileRatio;
    public int comboRatio;
    public int highScore { get { return _highScore; } }
    public int currentScore { get { return _currentScore; } }

    private int _currentScore;

    // Start is called before the first frame update
    void Start()
    {
        ResetCurrentScore();
    }

    public void ResetCurrentScore()
    {
        _currentScore = 0;
    }

    public void IncrementScore(int tileCount, int comboCount)
    {
        _currentScore += (tileCount * tileRatio) * (comboCount * comboRatio);

        SoundManager.Instance.PlayScoreBool(comboCount > 1);
    }

    public void setHighScore()
    {
        _highScore = _currentScore;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
