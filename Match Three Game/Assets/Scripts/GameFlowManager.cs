﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFlowManager : MonoBehaviour
{
    public UIGameOver gameOverUI;

    private static GameFlowManager _instance = null;

    public static GameFlowManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<GameFlowManager>();

                if(_instance == null)
                {
                    Debug.LogError("Fatal Error: Game Flow Manager not found");
                }
            }
            return _instance;
        }
    }

    public bool isGameOver
    {
        get
        {
            return _isGameOver;
        }
    }

    private bool _isGameOver = false;

    public void GameOver()
    {
        _isGameOver = true;
        ScoreManager.Instance.setHighScore();

        gameOverUI.Show();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
